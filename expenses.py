from flask import Flask, jsonify, abort, make_response, request, json
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

mysql_db_username = 'root'
mysql_db_password = 'p@55w0rd'
mysql_db_name = 'expense_management'
mysql_db_hostname = 'localhost'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://{DB_USER}:{DB_PASS}@{DB_ADDR}/{DB_NAME}".format(
    DB_USER=mysql_db_username,
    DB_PASS=mysql_db_password,
    DB_ADDR=mysql_db_hostname,
    DB_NAME=mysql_db_name)

db = SQLAlchemy(app)


class Expenses(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(20), nullable=False)
    category = db.Column(db.String(20), nullable=False)
    description = db.Column(db.String(50), nullable=False)
    link = db.Column(db.String(100), nullable=False)
    estimated_costs = db.Column(db.String(10), nullable=False)
    submit_date = db.Column(db.String(10), nullable=False)
    status = db.Column(db.String(20), nullable=False)
    decision_date = db.Column(db.String(250), nullable=False)

    def __init__(self, name, email, category, description, link, estimated_costs, submit_date, status, decision_date):
        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date


@app.route('/v1/expenses/<int:expense_id>', methods=['GET'])
def get_expense(expense_id):
    expense = Expenses.query.get(expense_id)
    if expense is None:
        abort(404)
    return jsonify(id=str(expense.id), name=expense.name, email=expense.email, category=expense.category,
                   description=expense.description, link=expense.link, estimated_costs=str(expense.estimated_costs),
                   submit_date=str(expense.submit_date), status=expense.status,
                   decision_date=str(expense.decision_date))


@app.route('/v1/expenses', methods=['POST'])
def create_expense():
    data = json.loads(request.data)
    expense = Expenses(data['name'], data['email'], data['category'],
                       data['description'], data['link'],
                       data['estimated_costs'], data['submit_date'], 'pending', '')
    db.session.add(expense)
    db.session.commit()
    newid = expense.id
    expense = Expenses.query.get(newid)
    return jsonify(id=str(expense.id), name=expense.name, email=expense.email, category=expense.category,
                   description=expense.description, link=expense.link, estimated_costs=str(expense.estimated_costs),
                   submit_date=str(expense.submit_date), status=expense.status,
                   decision_date=str(expense.decision_date)), 201


@app.route('/v1/expenses/<int:expense_id>', methods=['PUT'])
def update_expense(expense_id):
    data = json.loads(request.data)
    expense = Expenses.query.get(expense_id)
    expense.estimated_costs = data['estimated_costs']
    db.session.commit()
    return '', 202


@app.route('/v1/expenses/<int:expense_id>', methods=['DELETE'])
def delete_expense(expense_id):
    db.session.delete(Expenses.query.get(expense_id))
    db.session.commit()
    return '', 204


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == '__main__':
    app.run(debug=True)